<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;



class ConcertsController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://www.brindezinc.fr/spip.php?rubrique2',
            // You can set any number of default request options.
            'timeout'  => 2.0,
            
        ]);
        $response = $client->request('GET');
        $body = $response->getBody();
        $crawler = new Crawler((string) $body);

       $titles =  $crawler->filterXPath('//span[contains(@class, "titrearticle")]/a[1]')->extract(['_text']);
       $links =  $crawler->filterXPath('//span[contains(@class, "titrearticle")]/a[1]/@href')->extract(['_text']);

       for ($i=0; $i<5; $i++) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://www.brindezinc.fr/'.$links[$i],
            // You can set any number of default request options.
            'timeout'  => 2.0,
            
        ]);
        $response = $client->request('GET');
        $body = $response->getBody();
        $crawler = new Crawler((string) $body);

       $descriptions =  $crawler->filterXPath('//span[contains(@class, "texte")][2]')->extract(['_text']);
       }




        return $this->render('concerts/index.html.twig', [
            'controller_name' => 'ConcertsController',
            'body' => $body,
            'titles' => $titles,
            'links' => $links,
            'descriptions' => $descriptions
                    ]);
    }
}
